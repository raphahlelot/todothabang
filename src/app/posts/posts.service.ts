import { Injectable } from '@angular/core'
import { IPosts} from './posts'
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable ({
    providedIn: 'root'
})
export class PostsService {
    private PostsUrl = 'https://jsonplaceholder.typicode.com/posts'
    constructor(private http: HttpClient){}

      // getPosts(){
        //   return this.http.get(this.PostsUrl);
       //}
     getPosts(): Observable<IPosts[]>{
         return this.http.get<IPosts[]>(this.PostsUrl).pipe(
             tap(data => console.log('All: ' + JSON.stringify(data))),
             catchError(this.handleError)
        );
    }
    private handleError(err: HttpErrorResponse) {
        let errorMessage = '';
        if(err.error  instanceof ErrorEvent){
            errorMessage = `An error occured: ${err.status}, error message is: ${err.message}`;
        } else {
            errorMessage = `server returned code: ${err.status}, error message is: ${err.message}`;
        }
        console.log(errorMessage)
        return throwError(errorMessage);
    }

}