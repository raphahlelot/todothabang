import { Component, OnInit } from '@angular/core';
import { PostsService } from './posts.service';
import { IPosts } from './posts'

@Component({
   selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css'],
  providers: [PostsService]
})
export class PostsComponent implements OnInit {
 // private _postsService;
 errorMessage: string;

 filteredPosts: IPosts[];
  posts: IPosts [] = [];
  constructor(private postsService: PostsService) {
 //   this._postsService = postsService;
   }

  ngOnInit() : void  {
    //this.posts = this.postsService.getPosts[];
    this.filteredPosts = this.posts;
    console.log(this.posts)
  }

}
