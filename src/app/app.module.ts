import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { ConvertToSpacesPipe} from './shared/convert-to-spaces.pipes';


import { AppComponent } from './app.component';
import { TodoComponent } from './todo/todo.component';
import { PostsComponent } from './posts/posts.component';

@NgModule({
  declarations: [
    AppComponent,
    TodoComponent,
    PostsComponent,
    ConvertToSpacesPipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'Todo', component: TodoComponent},
      { path: 'posts', component: PostsComponent},
      { path: '', redirectTo: 'Todo', pathMatch: 'full'},
      { path: '**',redirectTo: 'Todo', pathMatch: 'full'},

    ])
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
